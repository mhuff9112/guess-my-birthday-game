from random import randint
print ("Welcome to Birthday Guesser!")
# Generate name.
name = input("Hi! What should i Call you? ")
print ("It's a pleasure to meet you, " + name +"!")

# Establishes intial basic ranges for Day Month and Year
day_high = 31
day_low = 1
month_high = 12
month_low = 1
year_high = 2022
year_low = 1900

# List for converting rand_month to Month Name
months = [
    "January, ",
    "February, ",
    "March, ",
    "April, ",
    "May, ",
    "June, ",
    "July, ",
    "August, ",
    "September, ",
    "October, ",
    "November, ",
    "December, "]

# Generates logic for Guesses
for guess_number in range(5):
    
        
    # Generates the Random Numbers for Month 
    rand_month = randint(month_low, month_high)

    # Converts the random number from rand_month into the Sting Name from List months
    rand_month_as_string = months[rand_month - 1]    

    #Generates the random numbers for Day and Year
    rand_year = randint(year_low, year_high)
    rand_day = randint(day_low, day_high)
    
    # Adjusts the Range of Day to account for months with different Day totals
    if rand_month == 4 or rand_month == 6 or rand_month == 9 or rand_month == 11:
        day_high = 30
    elif rand_month == 2:
        day_high = 28
    else:
        day_high = 31
        
    
    # Displays the guess generated by the random numbers     
    print("Guess " + str(guess_number+1) + ":" +
        "Is your birthday " + rand_month_as_string
        + str(rand_day) + " " + str(rand_year) + "?")
    
    # Askes User to confirm if guess is correct. 
    response = input("Yes or No? ")
    if response == "Yes":
        print("Woot I did it!")
        exit()
    
    # Checks to see if all 5 guesses have been completed and congradulates
    # the user for the guesser not getting the correct answer
    elif guess_number == 4:
        print("Wow! I still don't know. You Win!")
        exit()
    
    # Begins the logic for narrowing the ranges for more accurate guesses.
    else:
         # Askes the user if the Month was accurate and then adjusts the range
        # based on if the guess was Later or Earlier
        correcting_month = input("Was the Month Equal, Later, or Earlier? ")
        if correcting_month == "Equal":
            print("Great, I was close!")
            month_high = rand_month
            month_low = rand_month
            #print(month_high) Testing for stored month
            #print(month_low) Testing for stored month
        elif correcting_month == "Later":
            print("Sounds Good!")
            month_low = rand_month + 1
        else:
            print("Sounds Good!")
            month_high = rand_month - 1
            
        # Askes the user if the Day was accurate and then adjusts the range
        # based on if the guess was Later or Earlier
        correcting_day = input("Was the Day Equal, Later, or Earlier? ")
        if correcting_day == "Equal":
            print("Great, I was close!")
            day_high = rand_day
            day_low = rand_day
            #print(day_high) Testing for stored day
            #print(day_low) Testing for stored day
        elif correcting_day == "Later":
            print("Sounds Good!")
            day_low = rand_day + 1
        else:
            print("Sounds Good!")
            day_high = rand_day - 1
        
        # Askes the user if the Year was accurate and then adjusts the range
        # based on if the guess was Later or Earlier
        correcting_year = input("Was the Year Equal, Later, or Earlier? ")
        if correcting_year == "Equal":
            print("Great, I was close!")
            year_high = rand_year
            year_low = rand_year
            #print(year_high) Testing for stored year
            #print(year_low) Testing for stored year
        elif correcting_year == "Later":
            print("Sounds Good!")
            year_low = rand_year + 1
        else:
            print("Sounds Good!")
            year_high = rand_year - 1
